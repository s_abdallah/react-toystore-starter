/*
StyledToyWrapper - section
  display: flex;
  flex-wrap: wrap;
  margin: 2rem 0 0 2rem;

StyledToyButton - button
  border-color: ** couleur de bordure si sélectionné ou non **
  color: ** blanc **

  background: transparent;
  border-style: solid;
  border-width: 4px;
  cursor: pointer;
  display: inline-block;
  height: 8rem;
  font-size: 4rem;
  margin: 0 2rem 2rem 0;
  outline: none;
  transition: border-color ease 150ms;
  width: 8rem;
  .mdi {
    transition: opacity ease 150ms;
  }
  &:hover .mdi {
    opacity: 0.5;
  }
*/