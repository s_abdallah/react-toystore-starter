/*
  <List />
    <ToyList />

  - Appelle ToyList
  - Récupère la liste des produits au lancement du composant seulement
  - Gère la sélection d'un produit

  propTypes
    isLoaded
    toys
    selectToy
    getToys
*/
